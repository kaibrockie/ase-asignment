﻿
namespace ASE_Assignment
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.canvas = new System.Windows.Forms.Panel();
            this.SingleCommand = new System.Windows.Forms.TextBox();
            this.Commands = new System.Windows.Forms.RichTextBox();
            this.SCButton = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.Save = new System.Windows.Forms.Button();
            this.Load = new System.Windows.Forms.Button();
            this.saveText = new System.Windows.Forms.TextBox();
            this.loadText = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // canvas
            // 
            this.canvas.BackColor = System.Drawing.Color.Gray;
            this.canvas.Location = new System.Drawing.Point(419, 25);
            this.canvas.Name = "canvas";
            this.canvas.Size = new System.Drawing.Size(354, 270);
            this.canvas.TabIndex = 1;
            this.canvas.Paint += new System.Windows.Forms.PaintEventHandler(this.canvas_Paint);
            // 
            // SingleCommand
            // 
            this.SingleCommand.Location = new System.Drawing.Point(50, 365);
            this.SingleCommand.Name = "SingleCommand";
            this.SingleCommand.Size = new System.Drawing.Size(329, 27);
            this.SingleCommand.TabIndex = 2;
            this.SingleCommand.Text = "S";
            this.SingleCommand.TextChanged += new System.EventHandler(this.SingleCommand_TextChanged);
            this.SingleCommand.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SingleCommand_KeyDown);
            // 
            // Commands
            // 
            this.Commands.Location = new System.Drawing.Point(50, 25);
            this.Commands.Name = "Commands";
            this.Commands.Size = new System.Drawing.Size(329, 215);
            this.Commands.TabIndex = 3;
            this.Commands.Text = "";
            // 
            // SCButton
            // 
            this.SCButton.Location = new System.Drawing.Point(419, 365);
            this.SCButton.Name = "SCButton";
            this.SCButton.Size = new System.Drawing.Size(119, 27);
            this.SCButton.TabIndex = 4;
            this.SCButton.Text = "Run";
            this.SCButton.UseVisualStyleBackColor = true;
            this.SCButton.Click += new System.EventHandler(this.SCButon_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(50, 266);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(329, 29);
            this.button1.TabIndex = 5;
            this.button1.Text = "Run";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Save
            // 
            this.Save.Location = new System.Drawing.Point(50, 316);
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(94, 29);
            this.Save.TabIndex = 6;
            this.Save.Text = "Save";
            this.Save.UseVisualStyleBackColor = true;
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // Load
            // 
            this.Load.Location = new System.Drawing.Point(285, 316);
            this.Load.Name = "Load";
            this.Load.Size = new System.Drawing.Size(94, 29);
            this.Load.TabIndex = 7;
            this.Load.Text = "Load";
            this.Load.UseVisualStyleBackColor = true;
            this.Load.Click += new System.EventHandler(this.Load_Click);
            // 
            // saveText
            // 
            this.saveText.Location = new System.Drawing.Point(140, 317);
            this.saveText.Name = "saveText";
            this.saveText.Size = new System.Drawing.Size(125, 27);
            this.saveText.TabIndex = 8;
            // 
            // loadText
            // 
            this.loadText.Location = new System.Drawing.Point(376, 317);
            this.loadText.Name = "loadText";
            this.loadText.Size = new System.Drawing.Size(125, 27);
            this.loadText.TabIndex = 9;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.loadText);
            this.Controls.Add(this.saveText);
            this.Controls.Add(this.Load);
            this.Controls.Add(this.Save);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.SCButton);
            this.Controls.Add(this.Commands);
            this.Controls.Add(this.SingleCommand);
            this.Controls.Add(this.canvas);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel canvas;
        private System.Windows.Forms.TextBox SingleCommand;
        private System.Windows.Forms.RichTextBox Commands;
        private System.Windows.Forms.Button SCButton;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button Save;
        private System.Windows.Forms.Button Load;
        private System.Windows.Forms.TextBox saveText;
        private System.Windows.Forms.TextBox loadText;
    }
}

