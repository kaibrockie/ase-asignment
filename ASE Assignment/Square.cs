﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASE_Assignment
{
    class Square : Shape
    { 
    int length1;
    
    public Square() : base()
    {

    }

    public Square(Color colour, int x, int y, int length1) : base(colour, x, y)
    {
        this.colour = colour;
        this.length1 = length1;
       
    }

    public override double calcArea()
    {
        throw new NotImplementedException();
    }

    public override double calcPerimeter()
    {
        throw new NotImplementedException();
    }
    public override void draw(Graphics g)
    {
        Pen p = new Pen(colour);
        SolidBrush b = new SolidBrush(colour);
        //g.FillEllipse(b, x, y, radius * 2, radius * 2);
        g.DrawRectangle(p, x, y, x + length1*2, y + length1*2);
    }
}
}
