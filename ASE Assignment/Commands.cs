﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;

namespace ASE_Assignment
{
    class Commands
    {
        Graphics g;
        List<String> variable = new List<String>();
        List<String> varValue = new List<String>();


        public Commands() 
        { 
        }

        public void getCommand(Canvas myCanvas, string text)
        {
            string[] words = text.Split(' ');

            if (words[0] == "square")
            {
                try
                {
                    int length = Int32.Parse(words[1]);
                    myCanvas.DrawSquare(length);
                }
                catch
                {

                }
            }
            else if (words[0] == "rectangle")
            {
                try
                {

                    int length1 = Int32.Parse(words[1]);
                    int length2 = Int32.Parse(words[2]);
                    myCanvas.DrawRectangle(length1, length2);
                }
                catch
                {
                    System.Windows.Forms.MessageBox.Show("incorrect Parameters");
                }
            }
            else if (words[0] == "triangle")
            {
                try
                {

                    int length1 = Int32.Parse(words[1]);
                    int length2 = Int32.Parse(words[2]);
                    myCanvas.DrawRectangle(length1, length2);
                }
                catch
                {
                    System.Windows.Forms.MessageBox.Show("incorrect Parameters");
                }
            }
            else if (words[0] == "circle")
            {
                try
                {
                    int length = Int32.Parse(words[1]);
                    myCanvas.DrawCircle(length);
                }
                catch
                {
                    System.Windows.Forms.MessageBox.Show("incorrect Parameters");
                }
            }
            else if (words[0] == "moveto")
            {
                try
                {
                    int x = Int32.Parse(words[1]);
                    int y = Int32.Parse(words[2]);
                    myCanvas.MovePosition(x, y);
                }
                catch
                {
                    System.Windows.Forms.MessageBox.Show("incorrect Parameters");
                }
            }
            else if (words[0] == "drawto")
            {
                try
                {
                    int x = Int32.Parse(words[1]);
                    int y = Int32.Parse(words[2]);
                    myCanvas.drawLine(x, y);
                }
                catch
                {
                }
            }
            else if (words[0] == "pen")
            {
                try
                {
                    string colour = words[1];
                    string newColour = (char.ToUpper(colour[0]) + colour.Substring(1));
                    myCanvas.PenColour(newColour);
                }
                catch
                {
                    System.Windows.Forms.MessageBox.Show("incorrect Parameters");
                }
            }
            else if (words[0] == "fill")
            {
                myCanvas.FillToggle();
            }
            else if (words[0] == "clear")
            {
                myCanvas.clear();
            }
            else if (words[0] == "reset")
            {
                myCanvas.clear();
                myCanvas.reset();
            }
            //Part 2
            else
            {
                System.Windows.Forms.MessageBox.Show("incorrect Parameters");
            }
        }

    }
}
