﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ASE_Assignment
{
    public partial class Form1 : Form
    {
        ArrayList shapes = new ArrayList();
        Bitmap OutputBitmap = new Bitmap(640, 480);
        Canvas myCanvas;
        Factory factory;
        Commands myCommands;
        List<String> variable = new List<String>();
        List<String> varValue = new List<String>();

        public Form1()
        {
            InitializeComponent();
            myCanvas = new Canvas(Graphics.FromImage(OutputBitmap));
            myCommands = new Commands();
            factory = new Factory();
        }

        private void SingleCommand_KeyDown(object sender, KeyEventArgs e)
        {
            Console.WriteLine("key down");
        }

        private void SCButon_Click(object sender, EventArgs e)
        {
            string text = SingleCommand.Text.ToLower();
            string[] words = text.Split(' ');
            //myCommands.getCommand(myCanvas, text);
            if (words[0] == "thread" || words[0] == "rectangle" || words[0] == "red")
            {
                System.Windows.Forms.MessageBox.Show("Thread");
                factory.getShape(myCanvas, text);
            }
            else
            {
                shapes.Add(factory.getShape(myCanvas, text));
            }
            Refresh();
        }

        private void canvas_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.DrawImageUnscaled(OutputBitmap, 0, 0);

            for (int x = 0; x < shapes.Count; x++){
                Shape s;
                s = (Shape)shapes[x];
                s.draw(g);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int i = 0;
            int IF = 0;
            int IfNumber = 0;
            int L = 0;
            int l = 0;
            int W = 0;
            int I = 0;
            int M = 0;
            String MethodName = "";
            String Operator = "";
            String IfOperator = "";
            String IfVariable = "";
            String loopLength = "";
            String var = "";
            String text = Commands.Text.ToLower();

            List <String> normalLines = new List<String>();
            String[] lines =  text.Split(Environment.NewLine.ToCharArray());
            List<String> whileArray = new List<String>();
            List<String> MethodArray = new List<String>();
            List<String> IfArray = new List<String>();
            while (I < lines.Length)
            {   
                string[] words = lines[I].Split(' ');
                if (words[0] == "method")
                {
                    M = 1;
                    MethodName = words[1];
                    I++;
                }
                else if(M == 1 && words[0] != "endmethod")
                {
                    MethodArray.Add(lines[I]);
                    I++;
                }
                else if (words[0] == "endmethod")
                {
                    M = 0;
                    I++;
                }

                else if (words[0] == MethodName)
                {
                    foreach(String commands in MethodArray)
                    {
                        shapes.Add(factory.getShape(myCanvas, commands));
                    }
                    I++;
                }
                else if (words[0] == "if")
                {
                    
                    IF = 1;
                    IfVariable = words[1];
                    IfOperator = words[2];
                    IfNumber = Int32.Parse(words[3]);
                    I++;
                }
                else if (IF == 1 && words[0] != "endif")
                {
                    IfArray.Add(lines[I]);
                    System.Windows.Forms.MessageBox.Show("if added");
                    I++;
                }
                else if (words[0] == "endif")
                {
                    System.Windows.Forms.MessageBox.Show("endif");
                    while (IfVariable != variable[l])
                    {
                        l++;
                    }
                    System.Windows.Forms.MessageBox.Show(varValue[l]);
                    if (IfOperator == "==")
                    {
                        if(Int32.Parse(varValue[l]) == IfNumber)
                        {
                            System.Windows.Forms.MessageBox.Show("==");
                            foreach (String item in IfArray)
                            {
                                //myCommands.getCommand(myCanvas, item);
                                shapes.Add(factory.getShape(myCanvas, item));
                                Refresh();
                            }
                        }
                    }
                    I++;
                }
                else if (words[0] == "while")
                {
                    System.Windows.Forms.MessageBox.Show("while");
                    W = 1;
                    Operator = words[2];
                    var = words[1];
                    loopLength = words[3];
                    I++;
                }
                else if (W == 1 && words[0] != "endwhile")
                {
                    whileArray.Add(lines[I]);
                    I++;
                    
                }
                else if (words[0] == "endwhile")
                {
                    System.Windows.Forms.MessageBox.Show("endwhile initiated");
                    if (Operator == "<")
                    {
                        if (var != variable[L]) 
                        { 
                            while (var != variable[L])
                            {
                                L++;
                                System.Windows.Forms.MessageBox.Show(L.ToString());
                            }
                        
                        }
                        else
                        {
                            System.Windows.Forms.MessageBox.Show("works");
                            L = 0;
                        }
                        int num1 = Int32.Parse(varValue[L]);
                        int num2 = Int32.Parse(loopLength);
                        int loop = 0;
                        
                        
                        while(num1 < num2)
                        {
                            String[] whileItem = whileArray[loop].Split(' ');
                            if (whileItem[0] == var)
                            {
                                int num3 = Int32.Parse(whileItem[2]);
                                num1 = num1 + num3;
                                loop = 0;
                                
                            }
                            else if(whileItem[0] != var)
                            {
                                //myCommands.getCommand(myCanvas, whileArray[loop]);
                                if (whileItem[0] == "square" || whileItem[0] == "circle" || whileItem[0] == "rectangle")
                                {
                                    shapes.Add(factory.getShape(myCanvas, whileArray[loop]));
                                }
                                else
                                {
                                    factory.getShape(myCanvas, whileArray[loop]);
                                }
                                System.Windows.Forms.MessageBox.Show(whileItem[0]);
                                loop = loop + 1;
                                Refresh();
                            }
                        }
                    }
                    W = 0;
                    I++;
                }
                else if (M == 0 && W == 0 && words[1] != "=" && words[1] != "+")
                {
                    if (words[0] == "square" || words[0] == "circle" || words[0] == "rectangle")
                    {
                        System.Windows.Forms.MessageBox.Show("shape");
                        shapes.Add(factory.getShape(myCanvas, lines[I]));
                    }
                    else
                    {
                        factory.getShape(myCanvas, lines[I]);
                    }
                    //myCommands.getCommand(myCanvas, lines[I]);
                    I++;
                    Refresh();
                }
                
                
                else if (words[1] == "=")
                {
                   variable.Add(words[0]);
                   varValue.Add(words[2]);
                   factory.getShape(myCanvas, lines[I]);

                    System.Windows.Forms.MessageBox.Show("new var");
                    I++;
                }
                    else if (words[1] == "+")
                    {
                    factory.getShape(myCanvas, lines[I]);
                    while (variable[i] != words[0])
                        {
                            i++;
                        }
                        int prevValue = Int32.Parse(varValue[i]);
                        int addedValue = Int32.Parse(words[2]);
                        String newValue = (prevValue + addedValue).ToString();
                        varValue[i] = newValue;
                        I++;
                    }
                    else if (words[1] == "-")
                    {
                        while (variable[i] != words[0])
                        {
                            i++;
                        }
                        int prevValue = Int32.Parse(varValue[i]);
                        int addedValue = Int32.Parse(words[2]);
                        String newValue = (prevValue - addedValue).ToString();
                        varValue[i] = newValue;
                        I++;
                    }
            }

        }

        private void Save_Click(object sender, EventArgs e)
        { 
            string fileName = saveText.Text;
            if (fileName != "")
            {
                Commands.SaveFile(@$"C:\Users/Alex/Kai/{fileName}.txt", RichTextBoxStreamType.PlainText);
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("Enter File Name");
            }
        }

        private void Load_Click(object sender, EventArgs e)
        {
            
            
            string fileName = loadText.Text;
            if (fileName != "")
            {
                Commands.LoadFile(@$"C:\Users/Alex/Kai/{fileName}.txt", RichTextBoxStreamType.PlainText);
            }
            else
            {
               System.Windows.Forms.MessageBox.Show("Enter File Name");
            }
        }

        private void SingleCommand_TextChanged(object sender, EventArgs e)
        {

        }
    }
    }

