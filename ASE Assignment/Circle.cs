﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASE_Assignment
{
    class Circle : Shape
    {
        int radius;
        
        public Circle():base()
        {

        }

        public Circle(Color colour, int x, int y, int radius) : base(colour, x, y)
        {
            this.radius = radius;
            System.Windows.Forms.MessageBox.Show(radius.ToString());
        }

        public override double calcArea()
        {
            return Math.PI * radius;
        }

        public override double calcPerimeter()
        {
            return 2 * Math.PI * radius;
        }

        

        public override void draw(Graphics g)
        {
            Pen p = new Pen(Color.Black, 1);
            SolidBrush b = new SolidBrush(colour);
            //g.FillEllipse(b, x, y, radius * 2, radius * 2);
            g.DrawEllipse(p, x, y, radius, radius);
        }
    }
}
