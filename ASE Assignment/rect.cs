﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASE_Assignment
{
    class Rect : Shape
    {
        int length1;
        int length2;
        public Rect() : base()
        {

        }

        public Rect(Color colour, int x, int y, int length1, int length2): base(colour, x, y)
        {
            this.length1 = length1;
            this.length2 = length2;
        }

        public override double calcArea()
        {
            throw new NotImplementedException();
        }

        public override double calcPerimeter()
        {
            throw new NotImplementedException();
        }
        public override void draw(Graphics g)
        {
            Pen p = new Pen(Color.Black, 1);
            SolidBrush b = new SolidBrush(colour);
            //g.FillEllipse(b, x, y, radius * 2, radius * 2);
            g.DrawRectangle(p, x, y, x + length1, y + length2);
        }
    }
}
