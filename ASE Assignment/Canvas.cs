﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ASE_Assignment
{
    class Canvas
    {
        Graphics g;
        Pen Pen;
        Boolean running = false;
        Boolean flag = false;
        SolidBrush solidBrush;
        Thread newThread;
        int xPos, yPos;
        int fill;

        public Canvas(Graphics g)
        {
            this.g = g;
            xPos = yPos = 0;
            //newThread = new Thread(DrawThreadSquare);
            //newThread.Start();
            Pen = new Pen(Color.Red, 1);
            SolidBrush solidBrush = new SolidBrush(Color.Red);
            fill = 0 ;
        }

        public void DrawSquare(int width)
        {
            g.DrawRectangle(Pen, xPos, yPos, xPos + width, yPos + width);
            if (fill == 1)
            {
                Region rgn = new Region (new Rectangle(xPos, yPos, xPos + width, yPos + width));
                g.FillRegion(Brushes.Red, rgn);

            }
        }

        public void DrawRectangle(int width1, int width2)
        {

            g.DrawRectangle(Pen, xPos, yPos, xPos + width1, yPos + width2);
        }

        public void ThreadSquare(int width1)
        {
            g.DrawRectangle(Pen, xPos, yPos, xPos + width1, yPos + width1);
            Region rgn = new Region (new Rectangle(xPos, yPos, xPos + width1, yPos + width1*2));
            System.Windows.Forms.MessageBox.Show("Thread is on");
        }
        /*public void DrawThreadSquare()
        {
            System.Windows.Forms.MessageBox.Show("Thread is working");
            running = true;
            
            while (running == true)
            {
                if (flag == false)
                {
                    Region rgn = new Region(new Rectangle(xPos, yPos, xPos + 100, yPos + 100));
                    g.FillRegion(Brushes.Red, rgn);
                    flag = true;
                }
                else
                {
                    Region rgn = new Region(new Rectangle(xPos, yPos, xPos + 100, yPos + 100));
                    g.FillRegion(Brushes.Blue, rgn);
                    flag = false;
                }
                Thread.Sleep(500);
            }
        }*/

        public void DrawCircle(int width)
        {
            g.DrawEllipse(Pen, xPos-width, yPos-width, width*2, width*2);
        }
        public void drawLine(int x, int y)
        {
            PointF point1 = new PointF(xPos, yPos);
            PointF point2 = new PointF(x, y);
            g.DrawLine(Pen, point1, point2);
        }
        public void MovePosition(int x, int y)
        {
            xPos = x;
            yPos = y;
        }
        public void PenColour(string newColour)
        {
            Array colours = Enum.GetValues(typeof(KnownColor));
            KnownColor[] allColours = new KnownColor[colours.Length];

            Array.Copy(colours, allColours, colours.Length);
            for (int i = 0; i < allColours.Length; i++)
            {
                if (newColour == allColours[i].ToString())
                {
                    Pen.Color = Color.FromName(allColours[i].ToString());
                }
            }
        }

        public void FillToggle()
        {
            if(fill == 0)
            {
                fill = 1;
                System.Windows.Forms.MessageBox.Show("Fill is on");
            }
            else
            {
                fill = 0;
                System.Windows.Forms.MessageBox.Show("Fill is off");
            }
        }

        public void clear()
        {
            g.Clear(Color.Gray);
        }

        public void reset()
        {
            xPos = 0;
            yPos = 0;
        }


    }
}
