﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASE_Assignment
{
    class Factory
    {
        int x;
        int y;
        int thread = 0;
        ThreadClass myThread;

        List<String> variable = new List<String>();
        List<String> varValue = new List<String>();

        public Factory(){
            myThread = new ThreadClass();
            }

        public Shape getShape(Canvas myCanvas, String shape)
        {

            string[] words = shape.Split(' ');
            if (words[0] == "circle")
            {
                try
                {
                    String result = variable.FirstOrDefault(x => x == words[1]);
                    if (result != null)
                    {
                        int i = 0;
                        while (words[1] != variable[i])
                        {
                            i++;
                        }
                        return new Circle(Color.Red, x, y, Int32.Parse(varValue[i]));
                    }
                    else
                    {
                        return new Circle(Color.Red, x, y, Int32.Parse(words[1]));
                    }
                }
                catch
                {
                    return null;
                }
            }
            else if (words[0] == "rectangle")
            {
              return new Rect(Color.Red, x, y, Int32.Parse(words[1]), Int32.Parse(words[2]));
                
            }
            else if (words[0] == "square")
            {
                String result = variable.FirstOrDefault(x => x == words[1]);
                if (result != null)
                {
                    int i = 0;
                    while(words[1] != variable[i])
                    {
                        i++;
                    }
                    return new Square(Color.Pink, x, y, Int32.Parse(varValue[i]));
                }
                else
                {
                    return new Square(Color.Orange, x, y, Int32.Parse(words[1]));
                }
            }
            else if (words[0] == "moveto")
            {

                x = Int32.Parse(words[1]);
                y = Int32.Parse(words[2]);
                return null;
            }
            else if (words[1] == "=")
            {
                variable.Add(words[0]);
                varValue.Add(words[2]);
                return null;
            }
            else if (words[1] == "+")
            {
                int i = 0;
                while (variable[i] != words[0])
                {
                    i++;
                }
                int prevValue = Int32.Parse(varValue[i]);
                int addedValue = Int32.Parse(words[2]);
                String newValue = (prevValue + addedValue).ToString();
                varValue[i] = newValue;
                return null;
            }
            else
            {
                return null;
            }
        }

    }
}
