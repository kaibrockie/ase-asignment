using Microsoft.VisualStudio.TestTools.UnitTesting;
using ASE_Assignment;
using DocumentFormat.OpenXml.Office2010.CustomUI;
using System;

namespace ASE_Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            CommandParser c = new CommandParser();

            string command = "rectangle 100 50";
            string[] text = command.Split(' ');
            Assert.AreEqual(100, 100);
            Assert.AreEqual(50, 50);
        }
    }
}
